
import java.util.Scanner;

class FahrkartenautomatTest
{
	    static double gesamtPreis;
	    static double eingezahlterGesamtbetrag;
	    static double eingeworfeneM�nze;
	    static double zuZahlenderBetrag; 
	    static short anzahlTickets;
	    static double r�ckgabebetrag;
	    static Scanner tastatur = new Scanner(System.in);
        public static void main(String[] args)
    {
       ticketAusgabe(); 
       geldeinwurf(gesamtPreis);
       fahrscheinausgabe();
       wechselgeld();
       
      }
       // Ticketausgabe
    
     public static double ticketAusgabe()
     {
          System.out.print("Ticketpreis(Euro):" );
     zuZahlenderBetrag= tastatur.nextDouble();
       
       System.out.print("Anzahl der Tickets:");
       anzahlTickets = tastatur.nextShort();
       double gesamtPreis = zuZahlenderBetrag*anzahlTickets;
       return gesamtPreis;
     }
      
      
       // Geldeinwurf
       // -----------
    	 public static double geldeinwurf (double gesamtPreis)
    	 {
    		
       eingezahlterGesamtbetrag = 0.0;
       while(eingezahlterGesamtbetrag < gesamtPreis)
       {
    	   System.out.printf("Noch zu zahlen: " ); 
           System.out.printf("%.2f",anzahlTickets * zuZahlenderBetrag - eingezahlterGesamtbetrag);
           System.out.printf(" Euro");
    	   System.out.println("\nEingabe (mind. 5Ct, h�chstens 2 Euro): ");
    	   eingeworfeneM�nze = tastatur.nextDouble();
           eingezahlterGesamtbetrag += eingeworfeneM�nze;
       }
	return gesamtPreis;
    	 }
       // Fahrscheinausgabe
       // -----------------
     public static void fahrscheinausgabe(){
    	 {
     }
       System.out.println("\nFahrschein wird ausgegeben");
       for (int i = 0; i < 8; i++)
       {
          System.out.print("=");
          try {
			Thread.sleep(250);
		} catch (InterruptedException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
       }
       System.out.println("\n\n");
     }
     
       // R�ckgeldberechnung und -Ausgabe
       // -------------------------------
     public static double wechselgeld()
     {
       r�ckgabebetrag = eingezahlterGesamtbetrag - gesamtPreis;
       if(r�ckgabebetrag > 0.0)
       {
    	   System.out.printf("Der R�ckgabebetrag in H�he von " );
    	   System.out.printf ("%.2f",r�ckgabebetrag );
           System.out.println (" EURO ");
    	   System.out.printf("wird in folgenden M�nzen ausgezahlt:\n");

           while(r�ckgabebetrag >= 2.0) // 2 EURO-M�nzen
           {
        	  System.out.println("2 EURO");
	          r�ckgabebetrag -= 2.0;
           }
           while(r�ckgabebetrag >= 1.0) // 1 EURO-M�nzen
           {
        	  System.out.println("1 EURO");
	          r�ckgabebetrag -= 1.0;
           }
           while(r�ckgabebetrag >= 0.5) // 50 CENT-M�nzen
           {
        	  System.out.println("50 CENT");
	          r�ckgabebetrag -= 0.5;
           }
           while(r�ckgabebetrag >= 0.2) // 20 CENT-M�nzen
           {
        	  System.out.println("20 CENT");
 	          r�ckgabebetrag -= 0.2;
           }
           while(r�ckgabebetrag >= 0.1) // 10 CENT-M�nzen
           {
        	  System.out.println("10 CENT");
	          r�ckgabebetrag -= 0.1;
           }
           while(r�ckgabebetrag >= 0.05)// 5 CENT-M�nzen
           {
        	  System.out.println("5 CENT");
 	          r�ckgabebetrag -= 0.05;
           }
              }

       System.out.println("\nVergessen Sie nicht, den Fahrschein\n"+
                          "vor Fahrtantritt entwerten zu lassen!\n"+
                          "Wir w�nschen Ihnen eine gute Fahrt.");
	return anzahlTickets;
    }
    
}
    
// Ich habe mich f�r den Datentyp short entschieden, da man nur ganzzahlige Tickets kaufen kann und keiner mehr als 137 Tickets kauft
// Durch die Berechnung des Ausdruckes wird die Ticketanzahl mit dem Preis des Tickets verrechnet, sodass ein Gesamtbetrag entsteht