
public class ABAusgabenformatierung2 {

	public static void main(String[] args) {
		// TODO Auto-generated method stub

		//Aufgabe 1
		
		System.out.println("   **  ");
		System.out.println("*      *");
		System.out.println("*      *");
		System.out.println("   **  \n");
		
		//Aufgabe 2
		
		System.out.print(("0")+'!'); System.out.print("     =");  System.out.println("                   =   1");
		System.out.print(("1")+'!'); System.out.print("     ="); System.out.print("1"); System.out.println("                  =   1");
		System.out.print(("2")+'!'); System.out.print("     ="); System.out.print("1 * 2"); System.out.println("              =   2");
		System.out.print(("3")+'!'); System.out.print("     ="); System.out.print("1 * 2 * 3"); System.out.println("          =   6");
		System.out.print(("4")+'!'); System.out.print("     ="); System.out.print("1 * 2 * 3 * 4"); System.out.println("      =  24");
		System.out.print(("5")+'!'); System.out.print("     ="); System.out.print("1 * 2 * 3 * 4 * 5"); System.out.println("  = 120\n");
		
		// Aufgabe 3
		
		System.out.println(" Fahreneinheit | Celsius");
		System.out.println(" -----------------------");
		System.out.printf(" -20           | %.2f\n", -28.8889);
		System.out.printf(" -10           | %.2f\n", -23.3333);
		System.out.printf("  0            | %.2f\n", -17.7778);
		System.out.printf("  20           | %.2f\n", -6.6667);
		System.out.printf("  30           | %.2f\n", -1.1111);
	}

}
